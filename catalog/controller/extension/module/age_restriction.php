<?php
class ControllerExtensionModuleAgeRestriction extends Controller {
	public function index($setting = null) {
		if ($setting && $setting['status']) {
			$data = array();
			$data['message'] = sprintf($setting['message'], $setting['age']);
			$data['age'] = $setting['age'];
			$data['redirect_url'] = $setting['redirect_url'];
			return $this->load->view('extension/module/age_restriction', $data);
		}
	}
}